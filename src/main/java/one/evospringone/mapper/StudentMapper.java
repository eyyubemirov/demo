package one.evospringone.mapper;

import one.evospringone.dto.StudentDto;
import one.evospringone.model.Student;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StudentMapper {
      StudentMapper INSTANCE= Mappers.getMapper(StudentMapper.class);
    StudentDto mapEntityToDto(Student student);

    Student mapDtoToEntitiy(StudentDto studentDto);
}
