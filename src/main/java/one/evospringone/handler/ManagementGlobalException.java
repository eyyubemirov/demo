package one.evospringone.handler;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import one.evospringone.exception.ErrorResponseDto;
import one.evospringone.exception.NotFoundException;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
@Slf4j
public class ManagementGlobalException {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorResponseDto>handleNotFound(NotFoundException ex, HttpServletRequest request){
        log.info("Exception{}",ex.getLocalizedMessage());
        ex.printStackTrace();
        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .code("Not Found")
                .status(400)
                .detail("Can not found data")
                .message("Bad Request")
                .timestamp(LocalDateTime.now())
                .path(request.getRequestURI())
                .build());
    }
}
