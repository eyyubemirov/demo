package one.evospringone.repository;

import jakarta.persistence.LockModeType;
import one.evospringone.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Student> findById(Long id);



    List<Student> getByNameIgnoreCase(String name);

    List<Student> getByNameIgnoreCaseAndSurnameIgnoreCase(String name,String surname);

    @Query(value = "select * from student where id = ?",nativeQuery = true)
    Student getStudentById(Long id);

    @Query(value = "select * from student where age > ?",nativeQuery = true)
    List<Student> getStudentByAgeQuery(int age);
}
