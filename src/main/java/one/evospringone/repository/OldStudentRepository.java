package one.evospringone.repository;

import one.evospringone.model.OldStudent;
import one.evospringone.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OldStudentRepository extends JpaRepository<OldStudent,Long> {


}
