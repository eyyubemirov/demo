package one.evospringone.config;

import com.speedment.jpastreamer.application.JPAStreamer;
import jakarta.persistence.Column;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class JpaStreamerConfig {
   private final EntityManager entityManager;
   @Bean
   public JPAStreamer getJpa(){
       return JPAStreamer.of(entityManager.getEntityManagerFactory());
   }
}
