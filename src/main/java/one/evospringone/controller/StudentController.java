package one.evospringone.controller;

import jakarta.validation.Valid;
import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import one.evospringone.dto.StudentDto;
import one.evospringone.dto.StudentDtoRequest;
import one.evospringone.model.Student;
import one.evospringone.service.StudentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService service;

    @GetMapping("/getAll")
    public List<StudentDto> getAll() {
        return service.getAllDto();
    }











    @GetMapping("/getById")
    public StudentDto getByID(@RequestParam Long id) {
        return service.getById(id);
    }
    @GetMapping("/getByIdSleep")
    public StudentDto getByIDBySllep(@RequestParam Long id) {
        return service.getByIdBySleep(id);
    }

    @PutMapping("/{id}")
    public StudentDto updateStudent(@PathVariable Long id, @RequestBody Student student){
        return service.updateStudent(id,student);
    }

    @PutMapping("/{id}/sleep")
    public StudentDto updateStudentSleep(@PathVariable Long id, @RequestBody Student student){
        return service.updateStudentSleep(id,student);
    }







    @GetMapping("/getq")
    public StudentDto getStudentQuery(@PathParam("id")Long id){
        return service.getStudentQuery(id);
    }

    @GetMapping("/getname")
    public List<StudentDto> getByName(@RequestParam String name) {
        return service.getByName(name);
    }

    @GetMapping("/getfulln")
    public List<StudentDto> getByNameAndSurname(@RequestBody StudentDtoRequest dtoRequest) {
        return service.getByNameAndSurname(dtoRequest);
    }

    @PostMapping("/create")
    public StudentDto createDto(@Valid @RequestBody StudentDto dto) {
        return service.createDto(dto);
    }

    @DeleteMapping("delete/{id}")
    public void deleteById(@PathVariable Long id) {
        service.deleteById(id);
    }

    @PutMapping("update/{id}")
    public StudentDto update(@PathVariable Long id, @RequestBody StudentDto dto) {
        return service.update(id, dto);
    }
    @GetMapping("/getage")
    public List<StudentDto> getStudentByAgeQuery(@RequestParam int age){
        return service.getStudentByAgeQuery(age);
    }

    @GetMapping("/saveOld")
    public ResponseEntity<String> saveOldStudentAge( @RequestParam  int age){
      service.saveOldStudentAge(age);
      return ResponseEntity.ok("Data saved");
    }
}
