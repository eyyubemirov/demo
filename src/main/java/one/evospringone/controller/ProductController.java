package one.evospringone.controller;

import lombok.RequiredArgsConstructor;
import one.evospringone.model.Spesification.SearchCriteria;
import one.evospringone.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("product")
public class ProductController {
   private final ProductService service;
    @GetMapping("/criteria")
    public List getCriterial(@RequestBody List<SearchCriteria>criteria){
        return service.getCriterial(criteria);
    }
}
