package one.evospringone.service;

import one.evospringone.dto.StudentDto;
import one.evospringone.dto.StudentDtoRequest;
import one.evospringone.model.Student;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface StudentService {


    List<StudentDto> getAllDto();

    StudentDto getById(Long id);
    StudentDto getByIdBySleep(Long id);

    StudentDto createDto(StudentDto dto);

    void deleteById(Long id);

    StudentDto update(Long id, StudentDto dto);

    List<StudentDto> getByName(String name);

    List<StudentDto> getByNameAndSurname(StudentDtoRequest dtoResponse);

    StudentDto getStudentQuery(Long id);

    List<StudentDto> getStudentByAgeQuery(int age);

    void saveOldStudentAge(int age);

    StudentDto updateStudent(Long id, Student student);

    StudentDto updateStudentSleep(Long id, Student student);
}
