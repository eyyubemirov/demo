package one.evospringone.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import one.evospringone.model.Account;
import one.evospringone.repository.AccountRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AccountTransfer {
    private final AccountRepository accountRepository;

    @SneakyThrows
    @Transactional()
    public void transfer(Account account, Account account1, Double amount) {
        if (account.getBalance() < amount) {
            throw new RuntimeException("balans azdir");
        }
        Thread.sleep(10000);
        account.setBalance(account.getBalance() - amount);
        account1.setBalance(account1.getBalance() + amount);
        accountRepository.save(account);
        accountRepository.save(account1);


    }

}
