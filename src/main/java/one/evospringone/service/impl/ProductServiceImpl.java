package one.evospringone.service.impl;

import lombok.RequiredArgsConstructor;
import one.evospringone.model.Spesification.ProductSpecification;
import one.evospringone.model.Spesification.SearchCriteria;
import one.evospringone.repository.ProductRepository;
import one.evospringone.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    @Override
    public List getCriterial(List<SearchCriteria> criteria) {
        ProductSpecification productSpecification=new ProductSpecification();
        for (SearchCriteria criteria1:criteria){
            productSpecification.add(SearchCriteria.builder()
                    .key(criteria1.getKey())
                    .value(criteria1.getValue())
                    .operation(criteria1.getOperation())
                    .build());
        }
        return productRepository.findAll(productSpecification) ;
    }
}
