package one.evospringone.service.impl;

import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import one.evospringone.dto.StudentDto;
import one.evospringone.dto.StudentDtoRequest;
import one.evospringone.exception.NotFoundException;
import one.evospringone.mapper.StudentMapper;
import one.evospringone.model.OldStudent;
import one.evospringone.model.Student;
import one.evospringone.repository.OldStudentRepository;
import one.evospringone.repository.StudentRepository;
import one.evospringone.service.StudentService;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@Service
@Profile(value = {"evo2", "evo3", "evo4"})
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final OldStudentRepository oldStudentRepository;
    private final ModelMapper mapper;
    private final JPAStreamer jpaStreamer;
    private final StudentMapper studentMapper;

    public List<StudentDto> getAllDto() {
        if (true){
            throw new NotFoundException(":(");
        }
        List<Student> students = studentRepository.findAll();
        List<StudentDto> dtos = students.stream().map(studentMapper.INSTANCE::mapEntityToDto).collect(Collectors.toList());
        return dtos;
    }







    @Override
    @Transactional
    @Cacheable(cacheNames = "students",key = "#id")
    public StudentDto getById(Long id) {
        Student student=studentRepository.findById(id).orElseThrow(()->new NotFoundException("User tapilmadi"));

        return studentMapper.mapEntityToDto(student);
    }

    @Transactional
    @SneakyThrows
    public StudentDto getByIdBySleep(Long id) {

        Student student = studentRepository.findById(id).get();
        Thread.sleep(10000);

        return studentMapper.mapEntityToDto(student);
    }

    @Override
    @Transactional
    @CacheEvict(cacheNames = "students",key = "#id")
    public StudentDto updateStudent(Long id, Student student) {
        Student student1 = studentRepository.findById(id).get();
        student1.setName(student.getName());
        studentRepository.save(student1);
        return studentMapper.mapEntityToDto(student1);
    }

    @Override
    @SneakyThrows
    @Transactional
    public StudentDto updateStudentSleep(Long id, Student student) {
        Student student1 = studentRepository.findById(id).get();
        student1.setName(student.getName());
        Thread.sleep(10000);
        studentRepository.save(student1);
        return studentMapper.mapEntityToDto(student1);
    }










    public StudentDto createDto(StudentDto dto) {

        Student entitiy = studentMapper.mapDtoToEntitiy(dto);
        studentRepository.save(entitiy);
        StudentDto dto1 =studentMapper.mapEntityToDto(entitiy);

        return dto1;
    }

    @Override
    public void deleteById(Long id) {
        if (studentRepository.findById(id).isPresent()) {
            studentRepository.deleteById(id);
            System.out.println("Silindi:)");
        } else {
            throw new NotFoundException("Student Tapilmadi");
        }

    }

    @Override
    public StudentDto update(Long id, StudentDto dto) {
        //id olub olmamasini yoxlayir
        studentRepository.findById(id).orElseThrow(() -> new NotFoundException("Id uygun Student tapilmadi"));
        Student student = studentRepository.findById(id).orElseThrow(() -> new NotFoundException("Id uygun Student tapilmadi"));
        Student entitiy = studentMapper.mapDtoToEntitiy(dto);
        ;
        student.setName(entitiy.getName());
        student.setSurname(entitiy.getSurname());
        StudentDto scDto2 = studentMapper.mapEntityToDto(entitiy);

        return scDto2;
    }

    @Override
    public List<StudentDto> getByName(String name) {

        List<Student> students = studentRepository.getByNameIgnoreCase(name);
        List<StudentDto> dtos = students.stream().map(studentMapper.INSTANCE::mapEntityToDto).collect(Collectors.toList());
        return dtos;
    }

    @Override
    public List<StudentDto> getByNameAndSurname(StudentDtoRequest dtoRequest) {
        String name = dtoRequest.getName();
        String surname = dtoRequest.getSurname();
        List<Student> students = studentRepository.getByNameIgnoreCaseAndSurnameIgnoreCase(name, surname);
        List<StudentDto> dtos = students.stream().map(student -> mapper.map(student, StudentDto.class)).collect(Collectors.toList());
        return dtos;
    }

    @Override
    public StudentDto getStudentQuery(Long id) {

        return studentMapper.mapEntityToDto(studentRepository.getStudentById(id));
    }

    @Override
    public List<StudentDto> getStudentByAgeQuery(int age) {
        List<Student> students = studentRepository.getStudentByAgeQuery(age);
        List<StudentDto> dtos = students.stream().map(student -> mapper.map(student, StudentDto.class)).collect(Collectors.toList());
        return dtos;
    }

    @Override
    public void saveOldStudentAge(int age) {

        List<Student> students = jpaStreamer.stream(Student.class).filter(student -> student.getAge() > age).collect(Collectors.toList());
        List<OldStudent> oldStudents = students.stream().map(student -> mapper.map(student, OldStudent.class)).collect(Collectors.toList());
        oldStudentRepository.saveAll(oldStudents);

    }




}
