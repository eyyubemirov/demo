package one.evospringone;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import one.evospringone.model.Spesification.ProductSpecification;
import one.evospringone.model.Spesification.SearchCriteria;
import one.evospringone.model.Spesification.SearchOperation;
import one.evospringone.repository.AccountRepository;
import one.evospringone.repository.ProductRepository;
import one.evospringone.service.impl.AccountTransfer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@RequiredArgsConstructor
@EnableCaching
public class EvoSpringOneApplication implements CommandLineRunner {
private final ProductRepository productRepository;
private final AccountRepository accountRepository;
private final AccountTransfer transfer;
    public static void main(String[] args) {
        SpringApplication.run(EvoSpringOneApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        ProductSpecification productSpecification = new ProductSpecification();
//        productSpecification.add(SearchCriteria.builder()
//                .key("price")
//                .value(50)
//                .operation(SearchOperation.GREATER_THAN_EQUAL)
//                .build());
//        productSpecification.add(SearchCriteria.builder()
//                .key("price")
//                .value(70)
//                .operation(SearchOperation.LESS_THAN_EQUAL)
//                .build());
//        productRepository.findAll(productSpecification).forEach(System.out::println);


//        productSpecification.add(SearchCriteria.builder()
//                .key("count")
//                .value(120)
//                .operation(SearchOperation.LESS_THAN)
//                .build());
//        productRepository.findAll(productSpecification).forEach(System.out::println);




//
//        for (int i = 0; i <50 ; i++) {
//            Product product=Product.builder()
//                    .name("armud"+i)
//                    .price(BigDecimal.valueOf(50+i))
//                    .count((50+i)*2)
//                    .build();
//            productRepository.save(product);
   // }
        }
    }

